/**
          1 Опишіть своїми словами різницю між
            функціями setTimeout() і setInterval()`.
 
 setTimeout() дозволяє визвати функцію один раз через
   зазначенний час у функції
  setInterval() дозволяє регулярно визивати фунцію через
  заданий час у фукції
 

             2 Що станеться, якщо в функцію setTimeout() передати нульову затримку?
            Чи спрацює вона миттєво і чому?

  Так миттево але не першим 
приклад: 
 function showName() {
  console.log("Vasya");
}
setTimeout(showName, 0);

console.log("kolya");

Другий вариант Kolya покаже першим , а потім вариант с 
нульовою затримкою.

           3 Чому важливо не забувати викликати функцію clearInterval(),
           коли раніше створений цикл запуску вам вже не потрібен?

   Якщо не видаляти таймери, то на сайті можуть бути багі і за 
   можливих включень фукцій. 
 
 */

let image = document.querySelectorAll(".image-to-show");
let btnStop = document.getElementById("btnStop");
let btnPlay = document.getElementById("btnPlay");

let workingStop = true;
let currentImg = "0";
let imageSelected;
let timersArraay = [];

function showImage(a) {
  hideAllImages();
  for (let i = a; i < image.length; i++) {
    console.log(i);
    let tId = setTimeout(() => {
      hideAllImages();
      image[i].style.display = "";
      currentImg = i;
    }, (i - a) * 3000);
    timersArraay[i] = tId;
  }
}

function hideAllImages() {
  for (let i = 0; i < image.length; i++) {
    imageSelected = image[i];
    imageSelected.style.display = "none";
  }
}

showImage(0);

let showImg = setInterval(() => showImage(0), 12000);

btnStop.onclick = function () {
  for (timerId of timersArraay) {
    clearTimeout(timerId);
  }
  clearInterval(showImg);
};

btnPlay.onclick = function () {
  showImage(currentImg + 1);
  setTimeout(() => {
    showImage(0);
    showImg = setInterval(() => showImage(0), 12000);
  }, (3 - currentImg) * 3000);
};
